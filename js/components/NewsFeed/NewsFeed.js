var NewsFeed = (function () {

  function getNewsFeedData() {
    // Set up request parameters
    var params = {
      country: 'us',
      apiKey: '8e63316767f14a0bba6e4fbca94276e2'
    };

    // Create an ajax get request to obtain data
    $.ajax({
      url: "https://cors-anywhere.herokuapp.com/https://newsapi.org/v2/top-headlines",
      type: "GET",
      data: utils.toQueryString(params),
      success(data, text) {
        console.log('data', data);
      },
      error(request, status, error) {
        console.log('error', error)
      }
    });
  }

  function renderNewsFeed() {
    getNewsFeedData();
  }

  return {
    renderNewsFeed: renderNewsFeed
  };
})();

